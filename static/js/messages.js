function ChatRecode(){
    let obj = {};
    function setData(id, data){
        //保存某个人的聊天记录
        obj[id] = data;
    }
    function getData(id){
        let data = obj[id] ? obj[id] : '';
        return data;
    }
    function clearData(){
        obj = null;
    }
    function getMessages(num) {
        let keys = Object.keys(obj);
        //console.log(keys)
        let len = keys.length
        //console.log(keys[len - 1])
        //console.log(len)
        if(len > num){
            keys = keys.slice(len - num,len)
        }
        for(let i = 0;i < keys.length;i++){
            keys[i] = this.getData(keys[i])
        }
        return keys
    }
    return {
        setData,
        getData,
        clearData,
        getMessages
    }
}
