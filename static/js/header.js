document.writeln("<nav class='ui  attached segment'>");
document.writeln("    <div class='ui container'>");
document.writeln("        <div class='ui secondary menu stackable'>");
document.writeln("            <h1 class='ui header teal item'>PG</h1>");
/*document.writeln("            <a href='./index.html' class='m-item active m-mobile-hide item'><i class='home icon'></i>首页</a>");*/
document.writeln("            <a href='./index.html' class='m-item m-mobile-hide item'><i class='idea icon'></i>首页</a>");
// document.writeln("            <a href='#' class='m-item m-mobile-hide item'><i class='shopping cart icon'></i>商城</a>");
// document.writeln("            <a href='#' class='m-item m-mobile-hide item'><i class='clone icon'></i>赛事活动</a>");
document.writeln("            <a href='./communicate.html' class='m-item m-mobile-hide item' target='_blank'><i class='chat icon'></i>聊天</a>");
document.writeln("            <a href='#' class='m-item m-mobile-hide item'><i class='info icon'></i>关于我们</a>");
document.writeln("            <div class='m-mobile-hide right item'>");
document.writeln("                <div class='m-item ui icon  transparent input' id='nav-show-login' style='display: none;'>");
document.writeln("                    <a class='item' style='margin-right: 20px;' href='./notice.html'>");
document.writeln("                      <i class='icon mail'></i>");
document.writeln("                    <div class='tiny floating ui red label' id='notification-count' style='display: none'></div>");
document.writeln("                    </a>");
document.writeln("                    <a href='./post_input.html' style='line-height: 50px;'>发帖</a> ");
document.writeln("                    <div class='ui dropdown'>");
document.writeln("                        <a href='#' class='m-item item '>");
document.writeln("                            <img src='https://picsum.photos/id/1010/100/100' id='nav-avatar' class='ui avatar image'>");
document.writeln("                            <span id='nav-nickname'>关关雎鸠</span>");
document.writeln("                        </a>");
document.writeln("                        <div class='menu'>");
document.writeln("                            <div class='item'><i class='id card outline icon'></i>我的帖子</div>");
// document.writeln("                            <div class='item'>");
// document.writeln("                                <i class='dropdown icon'></i>");
// document.writeln("                                <span class='text'><i class='wrench icon'></i>设置</span>");
// document.writeln("                                <div class='menu'>");
// document.writeln("                                    <div class='item'>设置1</div>");
// document.writeln("                                    <div class='item'>设置2</div>");
// document.writeln("                                </div>");
// document.writeln("                            </div>");
// document.writeln("                            <div class='item'><i class='question circle outline icon'></i>帮助</div>");
// document.writeln("                            <div class='divider'></div>");
document.writeln("                            <div class='item' onclick='logout()'><i class='sign-out icon'></i>退出</div>");
document.writeln("                        </div>");
document.writeln("                    </div>");
document.writeln("                </div>");
document.writeln("                <div id='nav-show-logout' style='display: inline-flex;'>");
document.writeln("                    <a href='./login.html' class='m-item m-mobile-hide item'>登录</a> ");
document.writeln("                    <a href='./register.html' class='m-item m-mobile-hide item'>注册</a>");
document.writeln("                </div>");
document.writeln("            </div>");
document.writeln("        </div>");
document.writeln("    </div>");
document.writeln("    <a href='#' class='ui menu toggle  icon button m-right-top m-mobile-show'>");
document.writeln("        <i class='sidebar icon'>");
document.writeln("");
document.writeln("        </i>");
document.writeln("    </a>");
document.writeln("</nav>");


// 发送请求，获取用户信息
function getUserInfo() {
    $http({
        url: 'user/userInfo',
        method: 'GET'
    }).then(response => {
        // 拿到状态码，判断请求状态。
        const code = response.code
        // 这里如果code为200，证明用户已经登录并且成功
        if (code === 200) {
            // 隐藏登录注册按钮
            $('#nav-show-logout').css('display', 'none')
            // 显示用户信息区域
            $('#nav-show-login').css('display', 'inline-flex')
            // 渲染昵称和头像
            const userInfo = response.data
            $('#nav-avatar').attr("src", userInfo.avatar)
            $('#nav-nickname').text(userInfo.nickname)

            localStorage.setItem("userId", userInfo.id)
            localStorage.setItem("avatar", userInfo.avatar)
        } else {
            // 隐藏用户信息区域，显示登录注册按钮
            $('#nav-show-logout').css('display', 'inline-flex')
            $('#nav-show-login').css('display', 'none')

            localStorage.removeItem("userId")
            localStorage.removeItem("avatar")
        }
    }).catch(error => {
        console.log(error)
        localStorage.removeItem("userId")
        localStorage.removeItem("avatar")
    })
}

// 登出
function logout() {
    $http({
        url: 'auth/logout',
        method: 'DELETE'
    }).then(response => {
        location.reload()
    }).catch(error => {
        console.log(error)
    })
}

/**
 * 获取用户通知数量
 */
function getNotificationCount() {
    $http({
        url: 'forum/notification/count',
        method: 'GET'
    }).then(response => {
        const code = response.code
        if (code === 200) {
            // 获取通知数量
            const count = response.data
            if (count > 0) {
                $('#notification-count').css('display', 'block').html(count)
            } else {
                $('#notification-count').css('display', 'none')
            }
        }
    }).catch(error => {
        console.log(error)
    })
}

getUserInfo()
getNotificationCount()




















