// todo 环境配置 dev or pro
const env = 'dev'

let baseURL
if (env === 'dev') {
    // 开发环境
    baseURL = 'http://localhost:10001/v/'
} else if (env === 'pro') {
    // 生产环境
    baseURL = 'http://api.pt.mbcwdl.space/v/'
}

// 创建axios实例，添加默认配置
const $http = axios.create({
    baseURL: baseURL,
    timeout: 3000,
    withCredentials: true //开启跨域请求
});

// 配置响应拦截器
$http.interceptors.response.use(response => {
    return response.data
}, error => {
    console.log(error)
})
