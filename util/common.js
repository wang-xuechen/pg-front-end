//获取url参数内容
function GetQueryParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);//search,查询？后面的参数，并匹配正则
    if (r != null) return decodeURI(r[2]);
    return null;
}

/**
 * 拼接url上的query部分
 * 入参示例：
 * {
 *  id: 123,
 *  name: 'guan'
 * }
 * 返回示例：
 * id=123&name=guan
 * @param {JSON} params
 */
function buildQueryString(params) {
    let res = ''
    for (const key in params) {
            const element = params[key];
            res += key + "=" + element + "&"
    }
    return res.substr(0, res.length - 1)

}

/**
 * 探测是否登录
 * @param successCallBack
 * @param errorCallBack
 */
function isLogin(successCallBack, errorCallBack) {
    $http({
        url: 'auth/login/status',
        method: 'GET'
    }).then(response => {
        successCallBack(response)
    }).catch(error => {
        errorCallBack(error)
    })
}

/**
 * 时间格式化
 * @param fmt yyyy-MM-dd HH:mm:ss表示2019-06-06 19:45
 * @param date let date = new Date()
 * @returns {*}
 */
function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "y+": date.getFullYear().toString(),        // 年
        "M+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "m+": date.getMinutes().toString(),         // 分
        "s+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        };
    };
    return fmt;
}
